void main() {
  final square = Square(side: 10.0);
  print(square.area);
  final circle = Circle(radius: 5.0);
  print(circle.area);
}

abstract class Shape {
  double get area; //converted area method into computed property
}
  
class Square implements Shape {
  Square({this.side});
  final double side;
  double get area => side * side; //convert area methot to getter property
}
class Circle implements Shape {
  Circle({this.radius});
  final double radius;
  final double pi = 3.14;
  double get area => radius * radius * pi; //convert area methot to getter property
}