mixin BMI {
  double calculateBMI(double weight, double height) {
    return weight / (height * height);
  }
}
​
class Person with BMI {
  Person({this.name, this.age, this.height, this.weight});
  final String name;
  final int age;
  final double height;
  final double weight;
​
  double get bmi => calculateBMI(weight, height);
}
​
void main() {
  final person = Person(name: 'Putin', age: 66, height: 1.77, weight: 77.0);
  print(person.bmi);
}
​
Console
24.577867151840145
Documentation
Privacy notice Send feedback
no issues
Based on Flutter 1.20.0 Dart SDK 2.9.2